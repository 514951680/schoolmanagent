﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using School_BLL.ServiceProvider;
using School_Common.DTO;
namespace School_Mangement.Controllers
{
    public class BaseController : Controller
    {
        private static TeacherServiceProvider _TeacherSP;
        private static CommonServiceProvider _CommonSP;
        private static AdminServiceProvider _AdminSP;
        private static UserInfoDTO _UserInfo;
        private static StudentServiceProvider _StudentSP;
        // GET: Base
        protected void SetHeader(string menu, string subMenu, string page)
        {
            ViewBag.menu = menu;
            ViewBag.submenu = subMenu;
            ViewBag.page = page;
        }
        protected TeacherServiceProvider TeachersSP
        {
            get
            {
                if (_TeacherSP == null)
                    _TeacherSP = new TeacherServiceProvider();
                return _TeacherSP;
            }
        }
        protected CommonServiceProvider CommonSP
        {
            get
            {
                if (_CommonSP == null)
                    _CommonSP = new CommonServiceProvider();
                return _CommonSP;
            }
        }
        protected AdminServiceProvider AdminSP
        {
            get
            {
                if (_AdminSP == null)
                    _AdminSP = new AdminServiceProvider();
                return _AdminSP;
            }
        }
        public UserInfoDTO UserInfo
        {
            get { return _UserInfo; }
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(string.IsNullOrEmpty(User.Identity.Name))
            {
                filterContext.HttpContext.Response.Redirect("/Account/Login");
                
            }
            if (_UserInfo==null||(!string.IsNullOrEmpty(User.Identity.Name)&& _UserInfo !=null && User.Identity.Name !=_UserInfo.staff_num))
            {
                _UserInfo= CommonSP.GetUserInfo(User.Identity.Name);
            }
            
        }
        protected StudentServiceProvider StudentSP
        {
            get
            {
                if (_StudentSP == null)
                    _StudentSP = new StudentServiceProvider();
                return _StudentSP;
            }
        }

    }
}