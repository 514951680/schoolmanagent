﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using School_Common.DTO;
using School_BLL.ServiceProvider;
using School_Common.Resources;
using School_Common.Enum;
namespace School_Mangement.Controllers
{
    public class ClassController : BaseController
    {
        // GET: Class
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Management()
        {
            SetHeader(RexMenu.Home_DashBoard_Menu, RexMenu.Class_Management_Menu, "");
            return View();
        }
        public ActionResult ManagementClassSchedule()
        {
           
            return View();
        }
        //GalleryList
        public ActionResult GalleryListDetail()
        {
            SetHeader(RexMenu.Home_DashBoard_Menu, RexMenu.Class_Management_Menu, RexMenu.Class_Management_Photo);
            return View();
        }
    }
}