﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using School_Common.DTO;
using School_BLL.ServiceProvider;
using School_Common.Resources;
using School_Common.Enum;
using Microsoft.AspNet.Identity;
using School_Mangement.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;

namespace School_Mangement.Controllers
{
    public class TeacherController : BaseController
    {
        private ApplicationUserManager _userManager;
        public TeacherController()
        {
        }

        public TeacherController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Management()
        {
            SetHeader(RexMenu.Teacher_Management_Menu, RexMenu.Teacher_Management_SubMenu, RexMenu.Teacher_Management_ListPage);
            return View();
        }
        public ActionResult TeachersList([DataSourceRequest]DataSourceRequest request, string strKey)
        {
            var result = new List<TeacherDTO>();
            result = TeachersSP.GetVteachersList(strKey);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int teacherID)
        {
            SetHeader(RexMenu.Teacher_Management_Menu, RexMenu.Teacher_Management_SubMenu, RexMenu.Teacher_Management_EditPage);
            var result = TeachersSP.GetTeacherDetail(teacherID);
            Initddl();
           
            return View(result);
        }
        public ActionResult SaveEidtTeachers(TeacherDTO model)
        {
            try
            {
                if (TeachersSP.UpdateTeachers(model))
                {
                    return Json(new MessageDTO { is_success = true, message = model.staff_num, title = RexMessage.Msg_Success, redirection_url = "/Teacher/Management" });
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
                }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
            }

        }
        public ActionResult Create()
        {

            SetHeader(RexMenu.Teacher_Management_Menu, RexMenu.Teacher_Management_SubMenu, RexMenu.Teacher_Management_EditPage);
            Initddl();
            TeacherDTO teacherDTO = new TeacherDTO();
            teacherDTO.Graduates = new List<GraduatesDTO> ();
            teacherDTO.Experience = new List<ExperienceDTO> ();
            teacherDTO.Honour = new List<HonourDTO> ();
            teacherDTO.Punish = new List<PunishDTO>();
            return View(teacherDTO);
        }
        public async Task<ActionResult> SubmitTeachers(TeacherDTO model)
        {
            try  
            {
                if (TeachersSP.SaveTeachers(ref model))
                {
                    if (model.is_createuser)
                    {
                        var user = new ApplicationUser { UserName = model.staff_num, Email = model.email };
                        var result = await UserManager.CreateAsync(user, model.password);
                        if (result.Succeeded)
                        {
                            return Json(new MessageDTO { is_success = true, message = model.staff_num, title = RexMessage.Msg_Success, redirection_url = "/Teacher/Management" });
                        }
                        else
                        {
                            return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
                        }
                    }
                    else
                    {
                        return Json(new MessageDTO { is_success = true, message = model.staff_num, title = RexMessage.Msg_Success, redirection_url = "/Teacher/Management" });
                    }
                 
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
                }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
            }

        }
        public ActionResult DeleteTeacher(int teacherID)
        {
            try
            {
                if (TeachersSP.DeleteTeacher(teacherID))
                {
                    return Json(new MessageDTO { is_success = true, message = "", title = RexMessage.Msg_Success, redirection_url = "/Teacher/Management" });
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
                }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Teacher/Management" });
            }

        }
        public void Initddl()
        {
            var PoliticsList = CommonSP.GetSectionsItems(SectionsItemEnum.Politics.ToString()).ToList();
            ViewBag.PoliticsList = PoliticsList;
            var BloodTypeList = CommonSP.GetSectionsItems(SectionsItemEnum.BloodType.ToString()).ToList();
            ViewBag.BloodTypeList = BloodTypeList;
            var TeacherGradeList = CommonSP.GetSectionsItems(SectionsItemEnum.TeacherGrade.ToString()).ToList();
            ViewBag.TeacherGradeList = TeacherGradeList;
            var StaffTypeList = CommonSP.GetSectionsItems(SectionsItemEnum.StaffType.ToString()).ToList();
            ViewBag.StaffTypeList = StaffTypeList;

        }

       
     
    }
}