﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace School_Mangement.Controllers
{
    public class CommonController : BaseController
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetSelectionItems(string type)
        {
            var result = CommonSP.GetSectionsItems(type);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}