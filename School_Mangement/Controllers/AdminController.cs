﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using School_Common.Resources;
using School_Common.DTO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
namespace School_Mangement.Controllers
{
    public class AdminController : BaseController
    {

        // GET: Admin
        public ActionResult NoticeList()
        {
            SetHeader(RexMenu.Admin_Notice_Menu, RexMenu.Admin_Notice_SubMenu, RexMenu.Admin_Notice_ListPage);
            return View();
        }
        public ActionResult CreateNotice()
        {
            SetHeader(RexMenu.Admin_Notice_Menu, RexMenu.Admin_Notice_SubMenu, RexMenu.Admin_Notice_CreatePage);
            NoticeDTO model = new NoticeDTO();
            model.author = UserInfo.name;
            return View(model);
        }
        public ActionResult EditNotice(int newsID)
        {
            SetHeader(RexMenu.Admin_Notice_Menu, RexMenu.Admin_Notice_SubMenu, RexMenu.Admin_Notice_EditPage);
            var result = AdminSP.GetNoticeDetail(newsID);
            return View(result);
        }
        public ActionResult SubmitNotice(NoticeDTO model)
        {
            if (ModelState.IsValid)
            {
                AdminSP.SaveNotice(model);
                return Json(new MessageDTO { is_success = true, message = "", title = RexMessage.Msg_Success, redirection_url = "/Admin/NoticeList" });
            }
            else
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Admin/NoticeList" });
            }

        }
        public ActionResult UpdateNotice(NoticeDTO model)
        {
            if (ModelState.IsValid)
            {
                AdminSP.UpdateNotice(model);
                return Json(new MessageDTO { is_success = true, message = "", title = RexMessage.Msg_Success, redirection_url = "/Admin/NoticeList" });
            }
            else
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Admin/NoticeList" });
            }

        }
        public ActionResult GetNoticeList([DataSourceRequest]DataSourceRequest request)
        {
            var result = new List<NoticeDTO>();
            result = AdminSP.GetNoticeList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClassManagement()
        {
            return View();
        }
    }
}