﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using School_Common.Resources;
using School_Common.DTO;
using Kendo.Mvc.Extensions;
namespace School_Mangement.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Notice(int newsID)
        {        
            var result = AdminSP.GetNoticeDetail(newsID);
            return View(result);
        }
        public ActionResult DashBoard()
        {
            SetHeader(RexMenu.Home_DashBoard_Menu, RexMenu.Home_DashBoard_SubMenu,"");
            List<NoticeDTO> noticeModel = new List<NoticeDTO>();

            noticeModel=AdminSP.GetNoticeList().OrderByDescending(p => p.date).Take(10).ToList();
            ViewBag.NoticeList = noticeModel;
            return View();
        }
        public ActionResult GetTaskList([DataSourceRequest]DataSourceRequest request)
        {
            var result = new List<TaskDTO>();
            result.Add(new TaskDTO { app_num = "202009150001", app_name = "请假申请", created_by = "王传成", created_date = DateTime.Now });
            result.Add(new TaskDTO { app_num = "202009150002", app_name = "请假申请", created_by = "李晓梅", created_date = DateTime.Now });

            result.Add(new TaskDTO { app_num = "202009150003", app_name = "请假申请", created_by = "高嵩", created_date = DateTime.Now });

            result.Add(new TaskDTO { app_num = "202009150004", app_name = "费用申请", created_by = "高晓燕", created_date = DateTime.Now });

            result.Add(new TaskDTO { app_num = "202009150005", app_name = "项目申请", created_by = "王孝高", created_date = DateTime.Now });

            result.Add(new TaskDTO { app_num = "202009150006", app_name = "费用申请", created_by = "高嵩", created_date = DateTime.Now });

            result.Add(new TaskDTO { app_num = "202009150007", app_name = "请假申请", created_by = "吴勇", created_date = DateTime.Now });
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    }
}