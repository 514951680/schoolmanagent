﻿using School_Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using School_Common.DTO;
namespace School_Mangement.Controllers
{
    public class RequestFormController : BaseController
    {
        // GET: RequestForm
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateLeave()
        {
            return View();
        }
        public ActionResult CreateExpenseCliam()
        {
            ExpenseClaimDTO model = new ExpenseClaimDTO();
            model.details = new List<ExpenseClaimDetailsDTO>();
            Initddl();
            return View(model);
        }
        public ActionResult CreateProject()
        {
            return View();
        }
        public void Initddl()
        {      
            var releationship_type = CommonSP.GetSectionsItems(SectionsItemEnum.ExpenseClaimType.ToString()).ToList();
            ViewData["ExpenseClaimType"] = releationship_type;

        }
    }
}