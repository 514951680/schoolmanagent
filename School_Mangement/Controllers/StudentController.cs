﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using School_Common.DTO;
using School_Common.Enum;
using School_Common.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace School_Mangement.Controllers
{
    public class StudentController : BaseController
    {
        // GET: Students
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            SetHeader(RexMenu.Students_Management_SubMenu, RexMenu.Students_Notice_User, RexMenu.Students_Notice_Create);
            ViewData["ViewmodelType"] = "Create";
            Initddl();
            StudentsDTO model = new StudentsDTO();
            model.Graduates = new List<GraduatesDTO>();
            model.Honour = new List<HonourDTO>();
            model.Punish = new List<PunishDTO>();
            model.Parent = new List<ParentDTO>();
            model.register_date = DateTime.Now;
            return View(model);
        }

        public ActionResult Edit(int studentID)
        {
            SetHeader(RexMenu.Students_Management_SubMenu, RexMenu.Students_Notice_User, RexMenu.Students_Notice_EditPage);
            ViewData["ViewmodelType"] = "Edit";
            var result = StudentSP.GetStudentDetail(studentID);
            Initddl();

            return View(result);
        }
        public void Initddl()
        {
            var PoliticsList = CommonSP.GetSectionsItems(SectionsItemEnum.Politics.ToString()).ToList();
            ViewBag.PoliticsList = PoliticsList;
            var BloodTypeList = CommonSP.GetSectionsItems(SectionsItemEnum.BloodType.ToString()).ToList();
            ViewBag.BloodTypeList = BloodTypeList;
            var TeacherGradeList = CommonSP.GetSectionsItems(SectionsItemEnum.TeacherGrade.ToString()).ToList();
            ViewBag.TeacherGradeList = TeacherGradeList;
            var StaffTypeList = CommonSP.GetSectionsItems(SectionsItemEnum.StaffType.ToString()).ToList();
            ViewBag.StaffTypeList = StaffTypeList;
            var releationship_type = CommonSP.GetSectionsItems(SectionsItemEnum.ReleationshipType.ToString()).ToList();
            ViewData["releationship_type"] = releationship_type;

        }
        public ActionResult SubmitStudents(StudentsDTO model)
        {
            try
            {
                var studentModel = StudentSP.SaveStudents(model);
                if (studentModel!=null&& studentModel.student_id>0)
                {
                    return Json(new MessageDTO { is_success = true, message = studentModel.student_num, title = RexMessage.Msg_Success, redirection_url = "/Student/Management" });
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
            }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
            }

        }
        public ActionResult Management()
        {
            SetHeader(RexMenu.Students_Management_SubMenu, RexMenu.Students_Notice_User, RexMenu.Students_Management_ListPage);
            return View();
        }
        public ActionResult StudentsList( [DataSourceRequest]DataSourceRequest request, string strKey)
        {
            var result = new List<StudentsDTO>();
            result = StudentSP.GetVstudentsList(strKey);
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteStudent(int studentID)
        {
            try
            {
                if (StudentSP.DeleteStudent(studentID))
                {
                    return Json(new MessageDTO { is_success = true, message = "", title = RexMessage.Msg_Success, redirection_url = "/Student/Management" });
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
                }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
            }

        }
        public ActionResult SaveEidtStudents(StudentsDTO model)
        {
            try
            {
                if (StudentSP.UpdateStudents(model))
                {
                    return Json(new MessageDTO { is_success = true, message = model.student_num, title = RexMessage.Msg_Success, redirection_url = "/Student/Management" });
                }
                else
                {
                    return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
                }

            }
            catch (Exception ex)
            {
                return Json(new MessageDTO { is_success = false, message = RexMessage.Msg_DataInvalid, title = RexMessage.Msg_Failed, redirection_url = "/Student/Management" });
            }

        }

    }
}