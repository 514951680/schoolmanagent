﻿$(function () {
    $("form").kendoValidator();

})
$("#m_form").submit(function (e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
    mApp.blockPage();
    var form = $(this);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: form.serialize(), // serializes the form's elements.
        success: function (data) {
            mApp.unblockPage();
            if (data.is_success) {

                DialogMessage.AlertMessageAndRedirect(data.title, data.messag, data.redirection_url)
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var errorMessage = jqXHR.responseText;
            if (errorMessage.length > 0) {
                mApp.unblockPage();
            }
        }
    });

   
})
$("#btn_cancel").click(function(){
    window.location.href = '/Admin/NoticeList';

})
function EditNotice(id) {
    window.location.href = '/Admin/EditNotice?newsID=' + id;
}