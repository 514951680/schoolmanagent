﻿
function HonourIndex(dataItem) {
    var data = $("#HonourGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function PunishIndex(dataItem) {
    var data = $("#PunishGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function GraduatesIndex(dataItem) {
    var data = $("#GraduatesGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function ExperienceIndex(dataItem) {
    var data = $("#ExperienceGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}



function getFormData() {
    //Punish
    var Punish = [];
    var grid = $("#PunishGrid").data("kendoGrid");
    var data = grid.dataSource.view();
    for (var i = 0; i < data.length; i++) {
        Punish.push(data[i]);
    }
    //
    var Honour = [];
    var HGrid = $("#HonourGrid").data("kendoGrid");
    var Hdata = HGrid.dataSource.view();
    for (var i = 0; i < Hdata.length; i++) {
        Honour.push(Hdata[i]);
    }
    //Graduates
    var Graduates = [];
    var GGrid = $("#GraduatesGrid").data("kendoGrid");
    var Gdata = GGrid.dataSource.view();
    for (var i = 0; i < Gdata.length; i++) {
        Graduates.push(Gdata[i]);
    }
    //Experience
    var Experience = [];
    var EGrid = $("#ExperienceGrid").data("kendoGrid");
    var Edata = EGrid.dataSource.view();
    for (var i = 0; i < Edata.length; i++) {
        Experience.push(Edata[i]);
    }

    var postdata = {
        Honour: Honour,
        Punish: Punish,
        Graduates: Graduates,
        Experience: Experience,
        teacher_id: $("#teacher_id").val(),
        staff_num: $("#staff_num").val(),
        name: $("#name").val(),
        sex: $("#sex").val(),
        birth: $("#birth").val(),
        native_place: $("#native_place").val(),
        nation: $("#nation").val(),
        politics_id: $("#politics_id").val(),
        blood_type_id: $("#blood_type_id").val(),
        qq_num: $("#qq_num").val(),
        join_party_date: $("#join_party_date").val(),
        teacher_grade_id: $("#teacher_grade_id").val(),
        line_phone: $("#line_phone").val(),
        tel_phone_num: $("#tel_phone_num").val(),
        address: $("#address").val(),
        staff_type: $("#staff_type").val(),
        is_activity: $("#is_activity").is(":checked"),
        is_createuser: $("#is_createuser").is(":checked"),
        password: $("#password").val()

    }
    return postdata;
}
$("#btnSubmit").click(function (e) {
    var validatable = $("#Form").kendoValidator().data("kendoValidator");
   
    if (validatable.validate()) {
        
        if ($("#is_createuser").is(":checked")) {
            if ($("#password").val() == null || $("#password").val()=="") {
                $("#passwaring").show();
                e.preventDefault();
                return false;
            }
           
        } else
            $("#passwaring").hide();
        Submit();
    } else {
        DialogMessage.AlertMessage('创建失败,请完善资料!');
    }

    e.preventDefault();
})
function Submit() {
    
    DialogMessage.ConfirmMessage("是否保存当前教师信息", '', function () {
        mApp.blockPage();
        var data = getFormData();
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Teacher/SubmitTeachers',
            data: JSON.stringify(data),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.message, data.redirection_url);
                } else {
                    DialogMessage.AlertMessage(data.title, data.messag);
                    mApp.unblockPage(); 
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('创建失败!');
                mApp.unblockPage();
            }
        });
    });
}

$("#btnEdit").click(function (e) {
    var validatable = $("#Form").kendoValidator().data("kendoValidator");
    if (validatable.validate()) {
        Edit()
    } else {
        DialogMessage.AlertMessage('修改失败,请完善资料!');
    }

    e.preventDefault();
})

function Edit() {
    DialogMessage.ConfirmMessage("是否保存当前教师信息", '', function () {
        mApp.blockPage();
        var data = getFormData();
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Teacher/SaveEidtTeachers',
            data: JSON.stringify(data),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.message, data.redirection_url);
                } else {
                    DialogMessage.AlertMessage(data.title, data.messag);
                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('创建失败!');
                mApp.unblockPage();
            }
        });
    });
}
$("#btn_cancel").click(function (e) {
    window.location.href = '/Teacher/Management';
    e.preventDefault();

})
function EditTeacher(id) {
    window.location.href = '/Teacher/Edit?teacherID=' + id;
}

function DeleteTeacher(id) {
   
    var postdate = { teacherID: id }
    DialogMessage.ConfirmMessage("是否删除当前教师信息", '', function () {
        mApp.blockPage();
      
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Teacher/DeleteTeacher',
            data: JSON.stringify(postdate),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.messag, data.redirection_url)
                } else {
                    DialogMessage.AlertMessage(data.title, data.messag);
                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('删除失败!');
                mApp.unblockPage();
            }
        });
        mApp.unblockPage();
    });
}
function getKeyData() {
    return {
        strKey: $("#generalSearch").val()
    };
}
$("#generalSearch").bind('input propertychange', function () {
    var grid = $("#TeacherGrid").data("kendoGrid");
    grid.dataSource.read();
})