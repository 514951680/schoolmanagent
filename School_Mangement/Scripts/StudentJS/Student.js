﻿function SetAge() {
    var birth = $("#birth").data("kendoDatePicker").value();
    var d = new Date(birth);
    var AgeText = getAge(d.getFullYear(), (d.getMonth() + 1), d.getDate());
    $("#ageText").html(AgeText);
    $("#age").val(AgeText);

}
function getAge(birthYear, birthMonth, birthDay) {
    //定义返回值
    var returnAge


    //获取当前时间
    d = new Date();
    var nowYear = d.getFullYear()
    var nowMonth = d.getMonth() + 1
    var nowDay = d.getDate()

    //计算周岁年龄差
    if (nowYear == birthYear) {
        returnAge = 0; //同年 则为0岁
    } else {
        var ageDiff = nowYear - birthYear //年之差
        if (ageDiff > 0) {
            if (nowMonth == birthMonth) {
                var dayDiff = nowDay - birthDay //日之差
                if (dayDiff < 0) {
                    returnAge = ageDiff - 1
                } else {
                    returnAge = ageDiff
                }
            } else {
                var monthDiff = nowMonth - birthMonth //月之差
                if (monthDiff < 0) {
                    returnAge = ageDiff - 1
                } else {
                    returnAge = ageDiff
                }
            }
        } else {
            returnAge = -1 //输入有误
        }

    }
    return returnAge;
}

//function Submit() {
//    var validatable = $("#Form").kendoValidator().data("kendoValidator");
//    if (validatable.validate()) {
//    //DialogMessage.ConfirmMessage("是否保存当前教师信息", '', function () {
//        // mApp.blockPage();
//        $.ajax({
//            contentType: 'application/json; charset=utf-8',
//            dataType: 'json',
//            type: 'POST',
//            url: 'Submit',
//            data: $('#Form').serialize(),
//            success: function (data) {
//                if (data.Success) {
//                    swal({
//                        title: data.Title,
//                        text: data.Text
//                    })
//                    //swal({
//                    //    title: data.Title,
//                    //    text: data.Text
//                    //}).then(function () {
//                    //    window.opener = null;
//                    //    window.open('', '_self');
//                    //    window.close();
//                    //})
//                    mApp.unblockPage();
//                }
//                else {
//                    DialogMessage.AlertMessage(data.Title,
//                        data.Text);
//                    mApp.unblockPage();
//                }
//            },
//            failure: function (response) {
//                DialogMessage.AlertMessage('创建失败!');
//                mApp.unblockPage();
//            }
//        });
//    //});
//    } else {
//        DialogMessage.AlertMessage('创建失败,请完善资料!');
//    }
//}


function HonourIndex(dataItem) {
    var data = $("#HonourGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function PunishIndex(dataItem) {
    var data = $("#PunishGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function GraduatesIndex(dataItem) {
    var data = $("#GraduatesGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function ParentIndex(dataItem) {
    var data = $("#ParentGrid").data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}
function getFormData() {
    //Punish
    var Punish = [];
    var grid = $("#PunishGrid").data("kendoGrid");
    var data = grid.dataSource.view();
    for (var i = 0; i < data.length; i++) {
        Punish.push(data[i]);
    }
    //
    var Honour = [];
    var HGrid = $("#HonourGrid").data("kendoGrid");
    var Hdata = HGrid.dataSource.view();
    for (var i = 0; i < Hdata.length; i++) {
        Honour.push(Hdata[i]);
    }
    //Graduates
    //var Graduates = [];
    //var GGrid = $("#GraduatesGrid").data("kendoGrid");
    //var Gdata = GGrid.dataSource.view();
    //for (var i = 0; i < Gdata.length; i++) {
    //    Graduates.push(Gdata[i]);
    //}
    //Experience

    //Parent
    var Parent = [];
    var PGrid = $("#ParentGrid").data("kendoGrid");
    var Pdata = PGrid.dataSource.view();
    for (var i = 0; i < Pdata.length; i++) {
        Parent.push(Pdata[i]);
    }

    var postdata = {
        Honour: Honour,
        Punish: Punish,
        //Graduates: Graduates,
        Parent: Parent,

        student_id: $("#student_id").val(),
        student_num: $("#student_num").val(),
        name: $("#name").val(),
        sex: $("#sex").val(),
        birth: $("#birth").val(),
        native_place: $("#native_place").val(),
        nation: $("#nation").val(),
        politics_id: $("#politics_id").val(),
        blood_type_id: $("#blood_type_id").val(),
        //qq_num: $("#qq_num").val(),
        join_party_date: $("#join_party_date").val(),
        //teacher_grade_id: $("#teacher_grade_id").val(),
        //line_phone: $("#line_phone").val(),
        //tel_phone_num: $("#tel_phone_num").val(),
        address: $("#address").val(),
        is_activity: $("#is_activity").is(":checked"),
        //staff_type: $("#staff_type").val()

    }
    return postdata;
}
function SubmitForm() {
    var validatable = $("#Form").kendoValidator().data("kendoValidator");
    if (validatable.validate()) {
        Submit();
    } else {
        DialogMessage.AlertMessage('创建失败,请完善资料!');
    }

    
}
function Submit() {

    DialogMessage.ConfirmMessage("是否保存当前学生信息", '', function () {
        mApp.blockPage();
        var data = getFormData();
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Student/SubmitStudents',
            data: JSON.stringify(data),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.message, data.redirection_url)
                } else {
                    DialogMessage.AlertMessage(data.title, data.message);
                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('创建失败!');
                mApp.unblockPage();
            }
        });
    });
}


$("#btn_cancel").click(function (e) {
    window.location.href = '/Teacher/Management';
    e.preventDefault();

})
function EditStudent(id) {
    window.location.href = '/Student/Edit?studentID=' + id;
}

function DeleteStudent(id) {

    var postdate = { studentID: id }
    DialogMessage.ConfirmMessage("是否删除当前学生信息", '', function () {
        mApp.blockPage();

        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Student/DeleteStudent',
            data: JSON.stringify(postdate),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.messag, data.redirection_url)
                } else {
                    DialogMessage.AlertMessage(data.title, data.messag);
                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('删除失败!');
                mApp.unblockPage();
            }
        });
        mApp.unblockPage();
    });
}
function btnCancel() {
    window.location.href = "/Student/Management";
}
function EditForm() {
    var validatable = $("#Form").kendoValidator().data("kendoValidator");
    if (validatable.validate()) {
        Edit()
    } else {
        DialogMessage.AlertMessage('修改失败,请完善资料!');
    }
}
function Edit() {
    DialogMessage.ConfirmMessage("是否保存当前学生信息", '', function () {
        mApp.blockPage();
        var data = getFormData();
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: '/Student/SaveEidtStudents',
            data: JSON.stringify(data),
            success: function (data) {
                mApp.unblockPage();
                if (data.is_success) {

                    DialogMessage.AlertMessageAndRedirect(data.title, data.message, data.redirection_url)
                } else {
                    DialogMessage.AlertMessage(data.title, data.message);
                    mApp.unblockPage();
                }
            },
            failure: function (response) {
                DialogMessage.AlertMessage('创建失败!');
                mApp.unblockPage();
            }
        });
    });
}

function returnFalse() {
    return false;
}
function getKeyData() {

  
    return {
        strKey: $("#generalSearch").val()
    };
}
$("#generalSearch").bind('input propertychange', function () {
    var grid = $("#StudentGrid").data("kendoGrid");
    grid.dataSource.read();
})