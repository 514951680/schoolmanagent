﻿var DialogMessage = (function () {

    // declare private variables and/or functions

    return {
        // declare public variables and/or functions

        ConfirmMessage: function (title, text, confirmFunction) {
            swal({
                title: title,
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "是",
                cancelButtonText: "否",
                customClass:"sweetAlert",
            }).then(function (result) {
                if (result.value) {

                    confirmFunction();
                }

            });
        },

        ConfirmMessageUsingHTML: function (title, html, confirmFunction) {


            //console.log('ConfirmMessageUsingHTML');
            //console.log(html);

            html = html.replace('\n', '<br />');

            swal({
                title: title,
                html: html,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: YesText,
                cancelButtonText: NoText,
            }).then(function (result) {
                if (result.value) {
                    confirmFunction();
                }
            });
        },

        ConfirmMessageUsingHTMLKendoInputPopupForm: function (title, html, confirmFunction) {

            html = html.replace('\n', '<br />')

            swal({
                title: title,
                html: html,
                input: 'text',
                target: '.k-window',
                type: 'warning',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Save',
                cancelButtonText: 'Cancel',
            }).then(function (result) {
                if (result.value) {
                    //confirmFunction(`${result.value}`);
                    confirmFunction(result.value);
                }
            });
        },

        ConfirmMessageUsingHTMLKendoPopupForm: function (title, html, confirmFunction) {

            html = html.replace('\n', '<br />')

            swal({
                title: title,
                html: html,
                target: '.k-window',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: YesText,
                cancelButtonText: NoText,
            }).then(function (result) {
                if (result.value) {
                    confirmFunction();
                }
            });
        },
        AlertMessageAndRedirect: function (title, text, redirectUrl) {

            swal({
                title: title,
                text: text
            }).then(function () {
                location.href = redirectUrl;
            })
        },


        AlertMessage: function (title, text) {
            swal({
                type: 'warning',
                title: title,
                text: text,
                
            })
        },


        AlertMessageUsingHTML: function (title, text) {
            swal({
                type: 'warning',
                title: title,
                html: text,
                confirmButtonText: YesText
            })
        },

        AlertMessageKendoPopupForm: function (title, text) {
            swal({
                type: 'warning',
                title: title,
                text: text,
                target: '.k-window'
            })
        },

        AlertMessageKendoPopupTemplate: function (title, text) {
            swal({
                type: 'warning',
                title: title,
                text: text,
                target: '.windowtemplate'
            })
        },
        AlertMessageUsingHTMLKendoPopupForm: function (title, text) {
            swal({
                type: 'warning',
                title: title,
                html: text,
                target: '.k-window'
            })
        },

        AlertFormErrorMessage: function () {
            swal({
                title: '',
                html: 'There are some errors in your submission. Please <br /> correct them.',
                type: 'error',
            })
        }

    }

})();