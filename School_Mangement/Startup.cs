﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(School_Mangement.Startup))]
namespace School_Mangement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
