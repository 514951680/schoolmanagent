//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace School_DAL.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class classes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public classes()
        {
            this.class_students = new HashSet<class_students>();
            this.class_teachers = new HashSet<class_teachers>();
        }
    
        public int class_id { get; set; }
        public int grade_id { get; set; }
        public string class_name { get; set; }
        public Nullable<int> seq_num { get; set; }
        public bool is_activity { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<class_students> class_students { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<class_teachers> class_teachers { get; set; }
        public virtual grades grades { get; set; }
    }
}
