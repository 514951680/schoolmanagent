﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using School_DAL.Model;
namespace School_DAL.DALManage
{
    public class TeacherManage : BaseManage
    {
        public List<vteachers> GetVteachers()
        {
            using (var db = SchoolEntitites)
            {
                return db.vteachers.ToList();
            }
        }
        public teachers GetTeacherDetail(int teacherID)
        {

            using (var db = SchoolEntitites)
            {
                return db.teachers.Include("graduates").Include("experience").Include("honour_punish").Where(q => q.teacher_id == teacherID).FirstOrDefault();

            }
        }
        public bool SaveTeachers(teachers model)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (var db = SchoolEntitites)
                    {
                        db.teachers.Add(model);
                        db.SaveChanges();

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public bool UpdateTeachers(teachers model)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {

                    DeleteTeacherDetail(model.teacher_id);
                    using (var db = SchoolEntitites)
                    {
                        if (model.graduates.Count>0)
                        {
                            db.graduates.AddRange(model.graduates);
                        }
                        if (model.graduates.Count > 0)
                        {
                            db.experience.AddRange(model.experience);
                        }
                        if (model.graduates.Count > 0)
                        {
                            db.honour_punish.AddRange(model.honour_punish);
                        }
                        db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool DeleteTeacher(int teacherID)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    DeleteTeacherDetail(teacherID);
                    using (var db = SchoolEntitites)
                    {
                        var model = db.teachers.Where(q => q.teacher_id == teacherID).FirstOrDefault();
                        if (model!=null)
                        {
                            db.Entry(model).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                    }
                    ts.Complete();
                }
            }
            catch (Exception ex )
            {
                result = false;
            }
            return result;
        }

        public bool DeleteTeacherDetail(int teacherID)
        {
            bool result = true;
            try
            {
                List<experience> experiencelist = new List<experience>();
                List<graduates> graduateslist = new List<graduates>();
                List<honour_punish> honour_punishlist = new List<honour_punish>();
                using (TransactionScope ts = new TransactionScope())
                {
                    using (var db = SchoolEntitites)
                    {
                         experiencelist = db.experience.Where(q => q.teacher_id == teacherID).ToList();
                        foreach (var item in experiencelist)
                        {
                            db.Entry(item).State = EntityState.Deleted;
                        }
                         graduateslist = db.graduates.Where(q => q.teacher_id == teacherID).ToList();
                        foreach (var item in graduateslist)
                        {
                            db.Entry(item).State = EntityState.Deleted;
                        }
                         honour_punishlist = db.honour_punish.Where(q => q.teacher_id == teacherID).ToList();
                        foreach (var item in honour_punishlist)
                        {
                            db.Entry(item).State = EntityState.Deleted;
                        }
                        if (experiencelist.Count>0|| graduateslist.Count>0|| honour_punishlist.Count > 0)
                        {
                            db.SaveChanges();
                        }
                        

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public string GetStaffNum()
        {
            string StaffNum = string.Empty;
            using (var db = SchoolEntitites)
            {
                var list = db.teachers.ToList();
                if (list.Count > 0)
                {
                    StaffNum = list.OrderByDescending(q => q.teacher_id).FirstOrDefault().staff_num;
                    if (!string.IsNullOrEmpty(StaffNum))
                    {
                        if (StaffNum.Substring(2, 4) == DateTime.Now.Year.ToString())
                        {
                            StaffNum = StaffNum.Replace("ST", "");
                        }
                        else
                        {
                            StaffNum = StaffNum.Replace("ST", DateTime.Now.Year.ToString());
                        }

                        int Num = Convert.ToInt32(StaffNum) + 1;
                        StaffNum = "ST" + Convert.ToString(Num);
                    }
                }
                else
                {
                    StaffNum = "ST" + DateTime.Now.Year + "0001";
                }
            }
            return StaffNum;
        }
    }
}
