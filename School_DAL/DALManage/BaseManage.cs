﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_DAL.Model;
namespace School_DAL.DALManage
{
    public class BaseManage
    {
        private school_managementEntities _SchoolEntitites { get; set; }

        public school_managementEntities SchoolEntitites
        {
            get
            {
                this._SchoolEntitites = new school_managementEntities();

                return _SchoolEntitites;
            }
        }
       
    }
}
