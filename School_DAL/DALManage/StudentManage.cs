﻿using School_DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace School_DAL.DALManage
{
   public class StudentManage: BaseManage
    {
        public List<vstudents> GetVstudents()
        {
            using (var db = SchoolEntitites)
            {
                    return db.vstudents.ToList();
            }
        }
        public string GetStaffNum(string val)
        {
            string StaffNum = string.Empty;
            using (var db = SchoolEntitites)
            {
                var list = db.teachers.ToList();
                if (list.Count > 0)
                {
                    StaffNum = list.OrderByDescending(q => q.teacher_id).FirstOrDefault().staff_num;
                    if (!string.IsNullOrEmpty(StaffNum))
                    {
                        if (StaffNum.Substring(2, 4) == DateTime.Now.Year.ToString())
                        {
                            StaffNum = StaffNum.Replace(val, "");
                        }
                        else
                        {
                            StaffNum = StaffNum.Replace(val, DateTime.Now.Year.ToString());
                        }

                        int Num = Convert.ToInt32(StaffNum) + 1;
                        StaffNum = val + Convert.ToString(Num);
                    }
                }
                else
                {
                    StaffNum = val + DateTime.Now.Year + "0001";
                }
            }
            return StaffNum;
        }
        public students SaveStudents(students model)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (var db = SchoolEntitites)
                    {
                        model= db.students.Add(model);
                        db.SaveChanges();

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return model;
        }
        public parent SaveParent(parent model)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (var db = SchoolEntitites)
                    {
                        model = db.parent.Add(model);
                        db.SaveChanges();

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return model;
        }
        public students GetStudentDetail(int studentID)
        {

            using (var db = SchoolEntitites)
            {
                return db.students.Include("parent").Include("honour_punish").Where(q => q.student_id == studentID).FirstOrDefault();

            }
        }
        public bool DeleteStudent(int studentID)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    DeleteStudentDetail(studentID);
                    using (var db = SchoolEntitites)
                    {
                        var model = db.students.Where(q => q.student_id == studentID).FirstOrDefault();
                        if (model != null)
                        {
                            db.Entry(model).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public bool DeleteStudentDetail(int studentID)
        {
            bool result = true;
            try
            {
                List<parent> parentlist = new List<parent>();
                List<honour_punish> honour_punishlist = new List<honour_punish>();
                using (TransactionScope ts = new TransactionScope())
                {
                    using (var db = SchoolEntitites)
                    {
                        parentlist = db.parent.Where(q => q.student_id == studentID).ToList();
                        foreach (var item in parentlist)
                        {
                            db.Entry(item).State = EntityState.Deleted;
                        }
                        honour_punishlist = db.honour_punish.Where(q => q.student_id == studentID).ToList();
                        foreach (var item in honour_punishlist)
                        {
                            db.Entry(item).State = EntityState.Deleted;
                        }
                        if ( parentlist.Count > 0 || honour_punishlist.Count > 0)
                        {
                            db.SaveChanges();
                        }


                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        public bool UpdateStudents(students model)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {

                    DeleteStudentDetail(model.student_id);
                    using (var db = SchoolEntitites)
                    {
                        if (model.parent.Count > 0)
                        {
                            db.parent.AddRange(model.parent);
                        }
                        if (model.honour_punish.Count > 0)
                        {
                            db.honour_punish.AddRange(model.honour_punish);
                        }
                        db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                    }
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}
