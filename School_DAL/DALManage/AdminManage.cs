﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_DAL.Model;
using System.Transactions;
namespace School_DAL.DALManage
{
    public class AdminManage : BaseManage
    {
        public void SaveNotice(news notice)
        {
            using (TransactionScope ts = new TransactionScope())
            {
                using (var db = SchoolEntitites)
                {
                    if (notice.news_id == null)
                    {
                        db.news.Add(notice);

                    }
                    else
                    {
                        var model = db.news.Where(p => p.news_id == notice.news_id).FirstOrDefault();
                        if (model != null)
                        {
                            model.title = notice.title;
                            model.news_type_id = notice.news_type_id;
                            model.body = notice.body;
                            model.is_ontop = notice.is_ontop;
                            model.tags = notice.tags;
                        }
                        db.SaveChanges();

                    }
                }
                ts.Complete();
            }
        }
        public List<vnews> GetNoticeList()
        {
            using (var db = SchoolEntitites)
            {
                return db.vnews.ToList();
            }

        }
    }
}
