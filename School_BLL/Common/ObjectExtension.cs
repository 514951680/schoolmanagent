﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace School_BLL.Common
{
    public static class ObjectExtension
    {
        public static T ConvertTo<T>(this object sourceItem)
        {
            Type destItemType = typeof(T);
            T destItem = (T)Activator.CreateInstance(destItemType);

            if (sourceItem != null)
            {
                Type sourceItemType = sourceItem.GetType();

                foreach (PropertyInfo destItemProperty in destItemType.GetProperties())
                {
                    if (!destItemProperty.GetGetMethod().IsVirtual)
                    {
                        PropertyInfo sourceItemProperty = sourceItemType.GetProperty(destItemProperty.Name);
                        if (sourceItemProperty != null)
                            destItemProperty.SetValue(destItem, sourceItemProperty.GetValue(sourceItem));
                    }
                }
            }

            return destItem;
        }

        public static List<T> ConvertTo<T>(this Array sourceItemList)
        {
            List<T> list = new List<T>();

            if (sourceItemList != null)
            {
                foreach (object sourceItem in sourceItemList)
                {
                    T item = sourceItem.ConvertTo<T>();
                    list.Add(item);
                }
            }

            return list;
        }
    }
}
