﻿using School_BLL.Common;
using School_Common.DTO;
using School_DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_BLL.ServiceProvider
{
    public class StudentServiceProvider : BaseServiceProvider
    {
      
        public students SaveStudents(StudentsDTO model)
        {
            try
            {
                var studentsModel = model.ConvertTo<students>();
                //studentsModel.student_num = GetStaffNum("");
                List<parent> parent = new List<parent>();
                if (model.Parent != null)
                {
                    parent = model.Parent.ToArray().ConvertTo<parent>();
                }

                studentsModel.parent = parent;
                List<honour_punish> hplist = new List<honour_punish>();
                List<honour_punish> hlist = new List<honour_punish>();
                if(model.Honour != null)
                {
                    hlist = model.Honour.Select(p => { p.is_honour = true; return p; }).ToArray().ConvertTo<honour_punish>();
                    hplist.AddRange(hlist);
                }
                List<honour_punish> plist = new List<honour_punish>();
                if (model.Punish != null)
                {
                    plist= model.Punish.Select(p => { p.is_honour = false; return p; }).ToArray().ConvertTo<honour_punish>();
                    hplist.AddRange(plist);
                }
                          
                studentsModel.honour_punish = hplist;
                studentsModel = StudentDALManage.SaveStudents(studentsModel);
                //foreach (var item in parent)
                //{
                //    item.student_id = studentsModel.student_id;
                //    StudentDALManage.SaveParent(item);
                //}
                return studentsModel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public StudentsDTO GetStudentDetail(int studentID)
        {
            students Students = StudentDALManage.GetStudentDetail(studentID);
            StudentsDTO studentDTO = Students.ConvertTo<StudentsDTO>();
            studentDTO.Parent = Students.parent.ToArray().ConvertTo<ParentDTO>();
            studentDTO.Punish = Students.honour_punish.Where(q => q.is_honour == false).ToArray().ConvertTo<PunishDTO>();
            studentDTO.Honour = Students.honour_punish.Where(q => q.is_honour == true).ToArray().ConvertTo<HonourDTO>();
            return studentDTO;
        }
        public List<StudentsDTO> GetVstudentsList(string strKey)
        {
            if (!string.IsNullOrEmpty(strKey))
                return StudentDALManage.GetVstudents().Where(x => x.student_num.ToLower().Contains(strKey.ToLower()) || x.name.ToLower().Contains(strKey.ToLower())).ToArray().ConvertTo<StudentsDTO>();
            else
                return StudentDALManage.GetVstudents().ToArray().ConvertTo<StudentsDTO>();
        }
        public bool DeleteStudent(int studentID)
        {
            return StudentDALManage.DeleteStudent(studentID);
        }
        public bool UpdateStudents(StudentsDTO model)
        {
            var studentsModel = model.ConvertTo<students>();
            if (model.Parent != null)
            {
                List<parent> plist = model.Parent.Select(p => { p.student_id = model.student_id; return p; }).ToArray().ConvertTo<parent>();
                studentsModel.parent = plist;
            }
            List<honour_punish> hplist = new List<honour_punish>();
            if (model.Honour != null)
            {
                List<honour_punish> hlist = model.Honour.Select(p => { p.is_honour = true; p.student_id = model.student_id; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(hlist);
            }
            if (model.Honour != null)
            {

                List<honour_punish> plist = model.Punish.Select(p => { p.is_honour = false; p.student_id = model.student_id; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(plist);
            }
            studentsModel.honour_punish = hplist;
            return StudentDALManage.UpdateStudents(studentsModel);
        }
    }
}
