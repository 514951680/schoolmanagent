﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Common.DTO;
using School_BLL.Common;
using School_DAL.Model;

namespace School_BLL.ServiceProvider
{
    public class TeacherServiceProvider : BaseServiceProvider
    {
        public List<TeacherDTO> GetVteachersList(string strKey)
        {
            if (!string.IsNullOrEmpty(strKey))
                return TeacherDALManage.GetVteachers().Where(x => x.staff_num.ToLower().Contains(strKey.ToLower()) || x.name.ToLower().Contains(strKey.ToLower())).ToArray().ConvertTo<TeacherDTO>();
            else
                return TeacherDALManage.GetVteachers().ToArray().ConvertTo<TeacherDTO>();
        }
        public TeacherDTO GetTeacherDetail(int teacherID)
        {
            teachers teachers = TeacherDALManage.GetTeacherDetail(teacherID);
            TeacherDTO teacherDTO = teachers.ConvertTo<TeacherDTO>();
            teacherDTO.Graduates = teachers.graduates.ToArray().ConvertTo<GraduatesDTO>();
            teacherDTO.Experience = teachers.experience.ToArray().ConvertTo<ExperienceDTO>();
            teacherDTO.Punish = teachers.honour_punish.Where(q => q.is_honour == false).ToArray().ConvertTo<PunishDTO>();
            teacherDTO.Honour = teachers.honour_punish.Where(q => q.is_honour == true).ToArray().ConvertTo<HonourDTO>();
            return teacherDTO;
        }
        public bool SaveTeachers(ref TeacherDTO model)
        {
            var teachersModel = model.ConvertTo<teachers>();
            teachersModel.staff_num = GetStaffNum("ST");
            model.staff_num = teachersModel.staff_num;
            List<graduates> glist = new List<graduates>();
            if (model.Graduates != null)
            {
                glist = model.Graduates.ToArray().ConvertTo<graduates>();
            }
            teachersModel.graduates = glist;
            List<experience> elist = new List<experience>();
            if (model.Experience != null)
            {
                elist = model.Experience.ToArray().ConvertTo<experience>();
            }

            teachersModel.experience = elist;
            List<honour_punish> hplist = new List<honour_punish>();
            List<honour_punish> hlist = new List<honour_punish>();
            if (model.Honour != null)
            {
                hlist = model.Honour.Select(p => { p.is_honour = true; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(hlist);
            }
            List<honour_punish> plist = new List<honour_punish>();
            if (model.Punish != null)
            {
                plist = model.Punish.Select(p => { p.is_honour = false; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(plist);
            }
           ;

            teachersModel.honour_punish = hplist;
            return TeacherDALManage.SaveTeachers(teachersModel);
        }
        public bool UpdateTeachers(TeacherDTO model)
        {
            var teachersModel = model.ConvertTo<teachers>();
            if (model.Graduates != null)
            {
                List<graduates> glist = model.Graduates.Select(p => { p.teacher_id = model.teacher_id; return p; }).ToArray().ConvertTo<graduates>();
                teachersModel.graduates = glist;
            }
            if (model.Experience != null)
            {
                List<experience> elist = model.Experience.Select(p => { p.teacher_id = model.teacher_id; return p; }).ToArray().ConvertTo<experience>();
                teachersModel.experience = elist;
            }
            List<honour_punish> hplist = new List<honour_punish>();
            if (model.Honour != null)
            {
                List<honour_punish> hlist = model.Honour.Select(p => { p.is_honour = true; p.teacher_id = model.teacher_id; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(hlist);
            }
            if (model.Honour != null)
            {

                List<honour_punish> plist = model.Punish.Select(p => { p.is_honour = false; p.teacher_id = model.teacher_id; return p; }).ToArray().ConvertTo<honour_punish>();
                hplist.AddRange(plist);
            }
            teachersModel.honour_punish = hplist;
            return TeacherDALManage.UpdateTeachers(teachersModel);
        }
        public bool DeleteTeacher(int teacherID)
        {
            return TeacherDALManage.DeleteTeacher(teacherID);
        }


    }
}
