﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_DAL.DALManage;
using School_DAL.Model;
namespace School_BLL.ServiceProvider
{
    public class BaseServiceProvider
    {
        private static TeacherManage TeacherDAL;
        private static CommonManage CommonDAL;
        private static AdminManage AdminDAL;
        private static StudentManage StudentDAL;
        public static TeacherManage TeacherDALManage
        {
            get
            {
                TeacherDAL = new TeacherManage();

                return TeacherDAL;
            }
        }

        public static CommonManage CommonDALManage
        {
            get
            {
                CommonDAL = new CommonManage();

                return CommonDAL;
            }
        }
        public static AdminManage AdminDALManage
        {
            get
            {
                AdminDAL = new AdminManage();

                return AdminDAL;
            }
        }
        public static StudentManage StudentDALManage
        {
            get
            {
                StudentDAL = new StudentManage();

                return StudentDAL;
            }
        }
        public string GetStaffNum(string val)
        {
            return StudentDALManage.GetStaffNum(val);
        }
    }
}
