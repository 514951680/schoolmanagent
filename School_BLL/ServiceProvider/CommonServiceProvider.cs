﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Common.DTO;
using School_BLL.Common;
namespace School_BLL.ServiceProvider
{
    public class CommonServiceProvider : BaseServiceProvider
    {
        public List<SelectionItemDTO> GetSectionsItems(string type = "")
        {
            if (!string.IsNullOrEmpty(type))
            {
               return CommonDALManage.GetDBSelections().Where(p => p.ddl_type == type).ToArray().ConvertTo<SelectionItemDTO>();
            }
            else
            {
                return CommonDALManage.GetDBSelections().ToArray().ConvertTo<SelectionItemDTO>();
            }
        }
        public UserInfoDTO  GetUserInfo(string staffNum)
        {
            UserInfoDTO result = new UserInfoDTO();
            if (!string.IsNullOrEmpty(staffNum))
            {
                result = TeacherDALManage.GetVteachers().Where(p => p.staff_num == staffNum).ToArray().ConvertTo<UserInfoDTO>().FirstOrDefault();
            }
            return result;
        }


    }
}
