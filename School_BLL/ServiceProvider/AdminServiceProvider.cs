﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Common.DTO;
using School_BLL.Common;
using School_DAL.Model;
using System.Web;
namespace School_BLL.ServiceProvider
{
    public class AdminServiceProvider : BaseServiceProvider
    {
        public void SaveNotice(NoticeDTO model)
        {
            var newModel = model.ConvertTo<news>();
            newModel.created_date = DateTime.Now;
            newModel.modified_date = DateTime.Now;
            newModel.date = DateTime.Now;
            if (model.tags_list!=null &&model.tags_list.Count>0)
               newModel.tags= string.Join(",", model.tags_list.ToArray());
            AdminDALManage.SaveNotice(newModel);
        }
        public void UpdateNotice(NoticeDTO model)
        {
            var newModel = model.ConvertTo<news>();         
            newModel.modified_date = DateTime.Now;         
            if (model.tags_list != null && model.tags_list.Count > 0)
                newModel.tags = string.Join(",", model.tags_list.ToArray());
            AdminDALManage.SaveNotice(newModel);
        }
        public NoticeDTO GetNoticeDetail(int newsID)
        {
            var result= GetNoticeList().Where(p=>p.news_id== newsID).FirstOrDefault();
            result.tags_list = string.IsNullOrEmpty(result.tags)? new List<string>(): result.tags.Split(',').ToList();
            result.body = HttpUtility.HtmlDecode(result.body);
            return result;
        }
        public List<NoticeDTO> GetNoticeList()
        {
            return AdminDALManage.GetNoticeList().ToArray().ConvertTo<NoticeDTO>();
        }
    }
}
