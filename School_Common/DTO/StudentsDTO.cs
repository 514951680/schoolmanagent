﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class StudentsDTO
    {
        public int student_id { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "学生编号")]
        public string student_num { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [DisplayName("姓名")]
        public string name { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "出生日期")]
        public Nullable<System.DateTime> birth { get; set; }
        public Nullable<int> age { get; set; }
        public bool is_onlychild { get; set; }
        public bool sex { get; set; }
        public string sex_display
        {
            get
            {
                return sex == true ? "男" : "女";
            }
        }
        public string id_num { get; set; }
        public Nullable<int> blood_type_id { get; set; }
        public Nullable<System.DateTime> join_party_date { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "现居地址")]
        public string address { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "民族")]
        public string nation { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "籍贯")]
        public string native_place { get; set; }
        public Nullable<int> politics_id { get; set; }
        public Nullable<System.DateTime> register_date { get; set; }
        public string photo_path { get; set; }
        public bool is_activity { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }

        public IEnumerable<HonourDTO> Honour { get; set; }
        public IEnumerable<PunishDTO> Punish { get; set; }
        public IEnumerable<GraduatesDTO> Graduates { get; set; }
        public IEnumerable<ParentDTO> Parent { get; set; }
    }
}
