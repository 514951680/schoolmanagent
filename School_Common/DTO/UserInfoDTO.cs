﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class UserInfoDTO
    {
        public int staff_id
        {
            get { return teacher_id; }

        }

        public int teacher_id { get; set; }
        public string staff_num { get; set; }
        public string name { get; set; }
        public string phone_num { get; set; }
    }
}
