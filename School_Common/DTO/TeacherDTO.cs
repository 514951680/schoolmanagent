﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace School_Common.DTO
{
    public class TeacherDTO
    {
        public int teacher_id { get; set; }
       
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "职工名称")]
        public string name { get; set; }
        public string staff_num { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "手机号码")]
        [DataType(DataType.PhoneNumber)]
        public string tel_phone_num { get; set; }
        public bool sex { get; set; }
        public string sex_display
        {
            get
            {
                return sex == true ? "男" : "女";
            }
        }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "出生日期")]
        public Nullable<System.DateTime> birth { get; set; }
        public Nullable<int> age { get; set; }

        public string id_num { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "民族")]
        public string nation { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "籍贯")]
        public string native_place { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "政治面貌")]
        public int politics_id { get; set; }
        public Nullable<System.DateTime> join_party_date { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "血型")]
        public int blood_type_id { get; set; }
        public Nullable<System.DateTime> employee_date { get; set; }
        public Nullable<int> staff_type { get; set; }
        public string hobbies { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "现居地址")]
        public string address { get; set; }
        public string home_address { get; set; }
        public string email { get; set; }
        public string line_phone { get; set; }
        public Nullable<int> qq_num { get; set; }
        public bool is_activity { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string blood_type { get; set; }
        public string politics { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [Display(Name = "职称")]
        public int teacher_grade_id { get; set; }
        public string teacher_grade { get; set; }
        public bool is_createuser { get; set; }
        [Display(Name = "密码")]
        public string password { get; set; }
        public IEnumerable<HonourDTO> Honour { get; set; }
        public IEnumerable<PunishDTO> Punish { get; set; }
        public IEnumerable<GraduatesDTO> Graduates { get; set; }
        public IEnumerable<ExperienceDTO> Experience { get; set; }
    }
}


