﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class MessageDTO
    {
        public bool is_success { get; set; }
        public string  message { get; set; }
        public string title { get; set; }
        public string redirection_url { get; set; }
    }
}
