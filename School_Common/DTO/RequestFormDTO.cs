﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    class RequestFormDTO
    {
    }

    public class LeaveDTO
    {
        public string applicant_name { get; set; }
        public string applicant_num { get; set; }
        public DateTime app_date { get; set; }
        public string app_num { get; set; }
        public int leave_type { get; set; }
        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }

        public string leave_reason { get; set; }
    }

    public class ExpenseClaimDTO
    {
        public string applicant_name { get; set; }
        public string applicant_num { get; set; }
        public string app_num { get; set; }
        public DateTime app_date { get; set; }
        public List<ExpenseClaimDetailsDTO> details { get; set; }
        public string remark { get; set; }
    }
    public class ExpenseClaimDetailsDTO
    {
        public string detail_id { get; set; }
        [DataType(DataType.Date)]
        public DateTime date { get; set; }
        public int detail_type_id { get; set; }

        public decimal amount { get; set; }
        public string remark { get; set; }
    }
}
