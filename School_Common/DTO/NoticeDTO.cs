﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School_Common.Constant;
namespace School_Common.DTO
{
    public class NoticeDTO
    {
        public int news_id { get; set; }
        public int news_type_id { get; set; }
        public string news_type { get; set; }
        [Required(ErrorMessage = "{0}必须填写")]
        [DisplayName("标题")]
        public string title { get; set; }
        public string body { get; set; }
        public string HTML { get; set; }
        public string Url { get
            {
                return "/Home/Notice?newsID=" + news_id;
            }
                
        }
        public DateTime date { get; set; }
        public string display_date
        {
            get
            {
                return date.ToString(StaticValue.datefomat);
            }

        }
        public string display_datetime
        {
            get
            {
                return date.ToString(StaticValue.datetimefomat);
            }

        }
        public string author { get; set; }
        public List<string> tags_list { get; set; }
        public string tags { get; set; }
        public bool is_ontop { get; set; }
        public bool is_activity { get; set; }
        public DateTime created_date { get; set; }
        public DateTime modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
    }
}
