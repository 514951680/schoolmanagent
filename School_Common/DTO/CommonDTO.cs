﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class CommonDTO
    {
    }
   public class SelectionItemDTO
    {
        public int ddl_item_id { get; set; }
        public string ddl_type { get; set; }
        public string ddl_value { get; set; }
        public string ddl_text { get; set; }
        public Nullable<int> seq_num { get; set; }
        public bool is_activity { get; set; }

    }
}
