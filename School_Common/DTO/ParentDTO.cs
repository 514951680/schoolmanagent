﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class ParentDTO
    {
        public int parent_id { get; set; }
        public int student_id { get; set; }
        public string parent_name { get; set; }
        //[UIHint("DropDownListView")]
        //public SelectionItemDTO releationship_type { get; set; }
        [Display(Name = "关系")]
  
        public int releationship_type_id { get; set; }
        public int ddl_item_id {
            get {
                return releationship_type_id;
            }
            set
            {
                releationship_type_id = ddl_item_id;
            }
        }
        [UIHint("PhoneTemplate")]
        [DataType(DataType.PhoneNumber)]
        public string  cantact_num { get; set; }
        public string job_description { get; set; }
        public bool is_activity { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
    }
}
