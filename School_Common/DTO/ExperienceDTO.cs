﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
   public class ExperienceDTO
    {
        public int experience_id { get; set; }
        public int teacher_id { get; set; }
        public string company_name { get; set; }
        [Required]
        [Display(Name = "开始时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime from_date { get; set; }
        [Required]
        [Display(Name = "结束时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime to_date { get; set; }
        public string contact_person { get; set; }
        public Nullable<int> contact_num { get; set; }
        public bool is_activity { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
    }
}
