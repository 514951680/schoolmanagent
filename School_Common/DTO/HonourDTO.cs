﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
   public class HonourDTO
    {
        public int event_id { get; set; }
        [Required]
        [Display(Name = "发生时间")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime happen_date { get; set; }
        [Required]
        [Display(Name = "荣誉记录")]
        public string event_des { get; set; }
        public Nullable<int> student_id { get; set; }
        public Nullable<int> teacher_id { get; set; }
        public bool is_activity { get; set; }
        public bool is_honour { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }

    }
}
