﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.DTO
{
    public class TaskDTO
    {
        public string app_name { get; set; }
        public string app_num { get; set; }
        public string created_by { get; set; }
        public  DateTime created_date { get; set; }

    }
}
