﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Common.ViewModel
{
  public  class MessageViewModel
    {
        public bool Success { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public int ID { get; set; }
    }
}
